package com.example.umtepujcovna.objects;

import androidx.room.TypeConverter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;

public class Converters {
    @TypeConverter
    public static Date fromTimestamp(Long value) {
        return value == null ? null : new Date(value);
    }

    @TypeConverter
    public static Long dateToTimestamp(Date date) {
        return date == null ? null : date.getTime();
    }

    @TypeConverter
    public BigDecimal fromLong(Long value) {
        return value == null ? null : new BigDecimal(value).divide(new BigDecimal(100));
    }

    @TypeConverter
    public Long toLong(BigDecimal bigDecimal) {
        if (bigDecimal == null) {
            return null;
        } else {
            return bigDecimal.multiply(new BigDecimal(100)).longValue();
        }
    }

        @TypeConverter
        public static LocalDate toDate(String dateString) {
            if (dateString == null) {
                return null;
            } else {
                return LocalDate.parse(dateString);
            }
        }

        @TypeConverter
        public static String toDateString(LocalDate date) {
            if (date == null) {
                return null;
            } else {
                return date.toString();
            }
        }
}
