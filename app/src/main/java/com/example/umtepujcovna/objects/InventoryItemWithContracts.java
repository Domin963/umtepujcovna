package com.example.umtepujcovna.objects;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

public class InventoryItemWithContracts {
    @Embedded
    private InventoryItem item;

    @Relation(parentColumn = "inventory_id", entityColumn = "item_id", entity = Contract.class)
    private List<Contract> contractList;

    public InventoryItem getItem() {
        return item;
    }

    public void setItem(InventoryItem item) {
        this.item = item;
    }

    public List<Contract> getContractList() {
        return contractList;
    }

    public void setContractList(List<Contract> contractList) {
        this.contractList = contractList;
    }
}
