package com.example.umtepujcovna.objects;

import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity(tableName = "contracts")
public class Contract implements Serializable {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "contract_id")
    private int id;
    @ColumnInfo(name = "start_date")
    private LocalDate startDate;
    @ColumnInfo(name = "expected_end_date")
    private LocalDate expEndDate;
    @ColumnInfo(name = "end_date")
    private LocalDate endDate;
    @ColumnInfo(name = "customer_id")
    private int customerId;
    @Ignore
    private Client client;
    @ColumnInfo(name = "price")
    private BigDecimal price;
    private boolean returned;
    @ColumnInfo(name = "item_id")
    private int itemId;
    @ColumnInfo(name = "photo_uri_before")
    private String photoUriBefore;
    @ColumnInfo(name = "photo_uri_after")
    private String photoUriAfter;
    @ColumnInfo(name = "notes")
    private String notes;
    @Ignore
    private InventoryItem item;
    @Embedded
    private Address address;
    @Embedded
    @Nullable
    private Location location;

    public Contract(LocalDate startDate, LocalDate expEndDate, LocalDate endDate, int customerId, BigDecimal price, boolean returned, int itemId, String photoUriBefore, Address address, String notes) {
        this.startDate = startDate;
        this.expEndDate = expEndDate;
        this.endDate = endDate;
        this.customerId = customerId;
        this.price = price;
        this.returned = returned;
        this.itemId = itemId;
        this.photoUriBefore = photoUriBefore;
        this.address = address;
        this.notes = notes;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getExpEndDate() {
        return expEndDate;
    }

    public void setExpEndDate(LocalDate expEndDate) {
        this.expEndDate = expEndDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public boolean isReturned() {
        return returned;
    }

    public void setReturned(boolean returned) {
        this.returned = returned;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public String getPhotoUriBefore() {
        return photoUriBefore;
    }

    public void setPhotoUriBefore(String photoUriBefore) {
        this.photoUriBefore = photoUriBefore;
    }

    public String getPhotoUriAfter() {
        return photoUriAfter;
    }

    public void setPhotoUriAfter(String photoUriAfter) {
        this.photoUriAfter = photoUriAfter;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public InventoryItem getItem() {
        return item;
    }

    public void setItem(InventoryItem item) {
        this.item = item;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
