package com.example.umtepujcovna.objects;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity(tableName = "inventory_items")
public class InventoryItem implements Serializable {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "inventory_id")
    private int id;
    private String name;
    @ColumnInfo(name = "serial_number")
    private String serialNumber;
    private BigDecimal price;
    @ColumnInfo(name = "date_of_purchase")
    private LocalDate dateOfPurchase;
    @ColumnInfo(name = "photo_uri")
    private String photoURI;

    public InventoryItem() {
    }

    public InventoryItem(String name, String serialNumber, BigDecimal price, LocalDate dateOfPurchase, String photoURI) {
        this.name = name;
        this.serialNumber = serialNumber;
        this.price = price;
        this.dateOfPurchase = dateOfPurchase;
        this.photoURI = photoURI;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public LocalDate getDateOfPurchase() {
        return dateOfPurchase;
    }

    public void setDateOfPurchase(LocalDate dateOfPurchase) {
        this.dateOfPurchase = dateOfPurchase;
    }

    public String getPhotoURI() {
        return photoURI;
    }

    public void setPhotoURI(String photoURI) {
        this.photoURI = photoURI;
    }
}
