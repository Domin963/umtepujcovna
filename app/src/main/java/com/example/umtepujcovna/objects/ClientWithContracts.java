package com.example.umtepujcovna.objects;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

public class ClientWithContracts {
    @Embedded
    private Client client;

    @Relation(parentColumn = "client_id", entityColumn = "customer_id", entity = Contract.class)
    private List<Contract> contractList;

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public List<Contract> getContractList() {
        return contractList;
    }

    public void setContractList(List<Contract> contractList) {
        this.contractList = contractList;
    }
}
