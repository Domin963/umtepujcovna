package com.example.umtepujcovna.objects;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class Address implements Serializable {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "address_id")
    private int id;
    @ColumnInfo(name = "street_name")
    private String streetName;
    @ColumnInfo(name = "house_number")
    private int houseNumber;
    @ColumnInfo(name = "city")
    private String city;
    @ColumnInfo(name = "psc")
    private int psc;

    public Address(String streetName, int houseNumber, String city, int psc) {
        this.streetName = streetName;
        this.houseNumber = houseNumber;
        this.city = city;
        this.psc = psc;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public int getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(int houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getPsc() {
        return psc;
    }

    public void setPsc(int psc) {
        this.psc = psc;
    }
}
