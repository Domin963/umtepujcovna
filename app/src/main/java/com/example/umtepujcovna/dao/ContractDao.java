package com.example.umtepujcovna.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.umtepujcovna.objects.Contract;

import java.util.List;

@Dao
public interface ContractDao {
    @Insert
    void insert(Contract contract);

    @Delete
    void delete(Contract contract);

    @Update
    void update(Contract contract);

    @Query("SELECT * FROM contracts")
    List<Contract> getAll();

    @Query("SELECT * FROM contracts WHERE contract_id=:id")
    Contract getOne(int id);
}
