package com.example.umtepujcovna.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import com.example.umtepujcovna.objects.Client;
import com.example.umtepujcovna.objects.ClientWithContracts;

import java.util.List;

@Dao
public interface ClientDao {
    @Insert
    void insert(Client item);

    @Delete
    void delete(Client item);

    @Query("SELECT * FROM clients")
    List<Client> getAll();

    @Update
    void update(Client item);

    @Query("SELECT * FROM clients WHERE client_id=:id")
    Client getOne(int id);

    @Transaction
    @Query("SELECT * FROM clients")
    List<ClientWithContracts> getClientsWithContracts();

    @Transaction
    @Query("SELECT * FROM clients WHERE client_id=:id")
    List<ClientWithContracts> getOneClientWithContracts(int id);
}
