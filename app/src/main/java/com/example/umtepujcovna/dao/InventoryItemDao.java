package com.example.umtepujcovna.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import com.example.umtepujcovna.objects.InventoryItem;
import com.example.umtepujcovna.objects.InventoryItemWithContracts;

import java.util.List;

@Dao
public interface InventoryItemDao {
    @Insert
    void insert(InventoryItem item);

    @Delete
    void delete(InventoryItem item);

    @Query("SELECT * FROM inventory_items")
    List<InventoryItem> getAll();

    @Update
    void update(InventoryItem item);

    @Query("SELECT * FROM inventory_items WHERE inventory_id=:id")
    InventoryItem getOne(int id);

    @Transaction
    @Query("SELECT * FROM inventory_items")
    List<InventoryItemWithContracts> getInventoryItemsWithContracts();

    @Transaction
    @Query("SELECT * FROM inventory_items WHERE inventory_id=:id")
    List<InventoryItemWithContracts> getOneInventoryItemWithContracts(int id);

}
