package com.example.umtepujcovna;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.example.umtepujcovna.dao.ClientDao;
import com.example.umtepujcovna.dao.ContractDao;
import com.example.umtepujcovna.dao.InventoryItemDao;
import com.example.umtepujcovna.objects.Client;
import com.example.umtepujcovna.objects.Contract;
import com.example.umtepujcovna.objects.Converters;
import com.example.umtepujcovna.objects.InventoryItem;

@Database(entities = {InventoryItem.class, Contract.class, Client.class}, version = 4)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {
    public abstract InventoryItemDao inventoryItemDao();

    public abstract ClientDao clientDao();

    public abstract ContractDao contractDao();

    private static volatile AppDatabase INSTANCE;

    public static AppDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (AppDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase.class, "pujcovna").fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}
