package com.example.umtepujcovna.ui.inventory;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;

import com.example.umtepujcovna.AppDatabase;
import com.example.umtepujcovna.R;
import com.example.umtepujcovna.objects.InventoryItem;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class InventoryAddActivity extends AppCompatActivity {

    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final String TAG = InventoryAddActivity.class.getName();
    private String currentPhotoPath;
    private Toolbar myToolbar;
    private LocalDate localDate;
    private AppDatabase db = AppDatabase.getDatabase(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventory_add);
        myToolbar = findViewById(R.id.inventoryAddToolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle("Nová položka");
        EditText textDate = findViewById(R.id.inputDateOfPurchase);
        localDate = LocalDate.now();
        DatePickerDialog.OnDateSetListener datePicker = (view, year, monthOfYear, dayOfMonth) -> {
            localDate = localDate.withYear(year).withMonth(monthOfYear + 1).withDayOfMonth(dayOfMonth);
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
            textDate.setText(formatter.format(localDate));
        };

        textDate.setOnClickListener(v -> new DatePickerDialog(InventoryAddActivity.this, datePicker, localDate.getYear(), localDate.getMonth().getValue() - 1,
                localDate.getDayOfMonth()).show());

        //porizeni fotografie
        Button btnPhoto = findViewById(R.id.btnPhotoInventory);
        btnPhoto.setOnClickListener(v -> dispatchTakePictureIntent());
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Log.d(TAG, "An unrecoverable error occured while creating photo file");
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE & resultCode == RESULT_OK) {
            ImageView imageView = findViewById(R.id.imageViewInventory);
            // Get the dimensions of the View
            int targetW = imageView.getWidth();
            int targetH = imageView.getHeight();
            // Get the dimensions of the bitmap
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(currentPhotoPath, bmOptions);
            int photoW = bmOptions.outWidth;
            int photoH = bmOptions.outHeight;
            // Determine how much to scale down the image
            int scaleFactor = Math.max(1, Math.min(photoW / targetW, photoH / targetH));
            // Decode the image file into a Bitmap sized to fill the View
            bmOptions.inJustDecodeBounds = false;
            bmOptions.inSampleSize = scaleFactor;
            Bitmap bitmap = BitmapFactory.decodeFile(currentPhotoPath, bmOptions);
            imageView.setImageBitmap(bitmap);
        } else {
            Log.d(TAG, "PROBLEM WITH PHOTO");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_toolbar_save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_toolbar_save:
                // User chose the "save" item
                EditText textName = findViewById(R.id.inputName);
                EditText textSerial = findViewById(R.id.inputSerialNum);
                EditText textPrice = findViewById(R.id.inputPrice);
                EditText textDate = findViewById(R.id.inputDateOfPurchase);

                if (TextUtils.isEmpty(textName.getText())) {
                    textName.setError("Nutno vyplnit!");
                } else if (TextUtils.isEmpty(textSerial.getText())) {
                    textSerial.setError("Nutno vyplnit!");
                } else if (TextUtils.isEmpty(textPrice.getText())) {
                    textPrice.setError("Nutno vyplnit!");
                } else if (TextUtils.isEmpty(textDate.getText())) {
                    textDate.setError("Nutno vyplnit!");
                } else {
                    String name = textName.getText().toString();
                    String serial = textSerial.getText().toString();
                    BigDecimal price;
                    if (!textPrice.getText().toString().isEmpty()) {
                        price = new BigDecimal(textPrice.getText().toString());
                    } else {
                        price = BigDecimal.valueOf(0.0);
                    }
                    LocalDate date = LocalDate.parse(textDate.getText(), DateTimeFormatter.ofPattern("dd.MM.yyyy"));
                    InventoryItem newItem = new InventoryItem(name, serial, price, date, currentPhotoPath);
                    Thread netThread = new Thread(() -> db.inventoryItemDao().insert(newItem));
                    netThread.start();
                    finish();
                    return true;
                }
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }
}