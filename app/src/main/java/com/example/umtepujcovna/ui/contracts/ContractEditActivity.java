package com.example.umtepujcovna.ui.contracts;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import com.example.umtepujcovna.AppDatabase;
import com.example.umtepujcovna.R;
import com.example.umtepujcovna.objects.Address;
import com.example.umtepujcovna.objects.Client;
import com.example.umtepujcovna.objects.Contract;
import com.example.umtepujcovna.objects.InventoryItem;
import com.example.umtepujcovna.objects.Location;
import com.example.umtepujcovna.ui.clients.ClientListActivity;
import com.example.umtepujcovna.ui.inventory.InventoryListActivity;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Objects;

public class ContractEditActivity extends AppCompatActivity {
    private static final int REQUEST_IMAGE_CAPTURE_BEFORE = 3;
    private static final int REQUEST_IMAGE_CAPTURE_AFTER = 5;
    private static final String TAG = ContractEditActivity.class.getName();
    private static final int ACCESS_FINE_LOCATION_REQUEST_CODE = 4;
    private static final int LAUNCH_CLIENT_ACTIVITY = 1;
    private static final int LAUNCH_ITEM_ACTIVITY = 2;
    private Toolbar myToolbar;
    private AppDatabase db = AppDatabase.getDatabase(this);
    private TextView clientName, clientAddressStreetHouseNum, clientAddressCityPSC;
    private TextView itemName, itemSerialNum;
    private LocalDate dateOfContract, dateExpectedReturn, dateOfReturn;
    private EditText editDateOfContract, editDateExpectedReturn, editDateOfReturn;
    private EditText editStreet, editHouseNum, editCity, editPsc;
    private EditText editPrice, editNotes;
    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    private Contract contract;
    private String photoBeforeUri;
    private String photoAfterUri;
    private String photoBeforeUriNew;
    private String photoAfterUriNew;
    private ImageView imgViewBefore, imgViewAfter;
    private Client client;
    private InventoryItem inventoryItem;
    private FusedLocationProviderClient locationProviderClient;
    private Location myLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contract_edit);
        myToolbar = findViewById(R.id.contractEditToolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle("Upravit výpůjčku");
        contract = (Contract) getIntent().getExtras().getSerializable("contractToEdit");

        Button btnCustomer = findViewById(R.id.buttonEditSelectClient);
        btnCustomer.setOnClickListener(v -> {
            Intent i = new Intent(this, ClientListActivity.class);
            startActivityForResult(i, LAUNCH_CLIENT_ACTIVITY);
        });
        clientName = findViewById(R.id.contractEditClientName);
        clientAddressStreetHouseNum = findViewById(R.id.contractEditClientAddressStreetHouseNum);
        clientAddressCityPSC = findViewById(R.id.contractEditClientAddressCityPSC);
        clientName.setText(contract.getClient().getFirstName() + " " + contract.getClient().getLastName());
        clientAddressStreetHouseNum.setText(contract.getClient().getAddress().getStreetName() + " " + contract.getClient().getAddress().getHouseNumber());
        clientAddressCityPSC.setText(contract.getClient().getAddress().getCity() + " " + contract.getClient().getAddress().getPsc());

        Button btnInventoryItem = findViewById(R.id.buttonEditSelectInventoryItem);
        btnInventoryItem.setOnClickListener(v -> {
            Intent i = new Intent(this, InventoryListActivity.class);
            startActivityForResult(i, LAUNCH_ITEM_ACTIVITY);
        });
        itemName = findViewById(R.id.contractEditItemName);
        itemSerialNum = findViewById(R.id.contractEditItemSerialNum);
        itemName.setText(contract.getItem().getName());
        itemSerialNum.setText(contract.getItem().getSerialNumber());

        editDateOfContract = findViewById(R.id.inputContractEditDateOfContract);
        editDateExpectedReturn = findViewById(R.id.inputContractEditDateOfExpectedReturn);
        editDateOfReturn = findViewById(R.id.inputContractEditDateOfReturn);
        editDateOfContract.setText(formatter.format(contract.getStartDate()));
        editDateExpectedReturn.setText(formatter.format(contract.getExpEndDate()));
        if (!Objects.isNull(contract.getEndDate())) {
            editDateOfReturn.setText(formatter.format(contract.getEndDate()));
            editDateOfReturn.setVisibility(View.VISIBLE);
        }
        DatePickerDialog.OnDateSetListener datePicker1 = (view, year, monthOfYear, dayOfMonth) -> {
            dateOfContract = dateOfContract.withYear(year).withMonth(monthOfYear + 1).withDayOfMonth(dayOfMonth);
            editDateOfContract.setText(formatter.format(dateOfContract));
        };
        DatePickerDialog.OnDateSetListener datePicker2 = (view, year, monthOfYear, dayOfMonth) -> {
            dateExpectedReturn = dateExpectedReturn.withYear(year).withMonth(monthOfYear + 1).withDayOfMonth(dayOfMonth);
            editDateExpectedReturn.setText(formatter.format(dateExpectedReturn));
        };
        DatePickerDialog.OnDateSetListener datePicker3 = (view, year, monthOfYear, dayOfMonth) -> {
            dateOfReturn = dateOfReturn.withYear(year).withMonth(monthOfYear + 1).withDayOfMonth(dayOfMonth);
            editDateOfReturn.setText(formatter.format(dateOfReturn));
        };
        dateOfContract = contract.getStartDate();
        editDateOfContract.setOnClickListener(v -> new DatePickerDialog(this, datePicker1, dateOfContract.getYear(), dateOfContract.getMonth().getValue() - 1,
                dateOfContract.getDayOfMonth()).show());
        dateExpectedReturn = contract.getExpEndDate();
        editDateExpectedReturn.setOnClickListener(v -> new DatePickerDialog(this, datePicker2, dateExpectedReturn.getYear(), dateExpectedReturn.getMonth().getValue() - 1,
                dateExpectedReturn.getDayOfMonth()).show());
        if (!Objects.isNull(contract.getEndDate())) {
            dateExpectedReturn = contract.getEndDate();
            editDateOfReturn.setOnClickListener(v -> new DatePickerDialog(this, datePicker3, dateOfReturn.getYear(), dateOfReturn.getMonth().getValue() - 1,
                    dateOfReturn.getDayOfMonth()).show());
            TextView dateOfReturnLabel = findViewById(R.id.contractEditDateOfReturnLabel);
            dateOfReturnLabel.setVisibility(View.VISIBLE);
            editDateOfReturn.setVisibility(View.VISIBLE);
        }

        editPrice = findViewById(R.id.inputContractEditPrice);
        editPrice.setText(contract.getPrice().toString());
        imgViewBefore = findViewById(R.id.contractEditImgBeforeView);
        if (!Objects.isNull(contract.getPhotoUriBefore())) {
            photoBeforeUri = contract.getPhotoUriBefore();
            Button btnPhotoBefore = findViewById(R.id.buttonContractEditPhotoBefore);
            btnPhotoBefore.setOnClickListener(v -> dispatchTakePictureIntent(REQUEST_IMAGE_CAPTURE_BEFORE));
            setThumbnail(imgViewBefore, photoBeforeUri);
        } else {
            imgViewBefore.setVisibility(View.GONE);
        }
        if (!Objects.isNull(contract.getPhotoUriAfter())) {
            photoAfterUri = contract.getPhotoUriAfter();
            Button btnPhotoAfter = findViewById(R.id.buttonContractEditPhotoAfter);
            btnPhotoAfter.setOnClickListener(v -> dispatchTakePictureIntent(REQUEST_IMAGE_CAPTURE_AFTER));
            btnPhotoAfter.setVisibility(View.VISIBLE);
            imgViewAfter = findViewById(R.id.contractEditImgAfterView);
            imgViewAfter.setVisibility(View.VISIBLE);
            setThumbnail(imgViewAfter, photoAfterUri);
        }
        editStreet = findViewById(R.id.inputContractEditLocationStreet);
        editHouseNum = findViewById(R.id.inputContractEditLocationHouseNum);
        editCity = findViewById(R.id.inputContractEditLocationCity);
        editPsc = findViewById(R.id.inputContractEditLocationPsc);
        //kdyz mam adresu (=nemam gps)
        if (!Objects.isNull(contract.getAddress())) {
            editStreet.setText(contract.getAddress().getStreetName());
            editStreet.setVisibility(View.VISIBLE);
            editHouseNum.setText(Integer.toString(contract.getAddress().getHouseNumber()));
            editHouseNum.setVisibility(View.VISIBLE);
            editCity.setText(contract.getAddress().getCity());
            editCity.setVisibility(View.VISIBLE);
            editPsc.setText(Integer.toString(contract.getAddress().getPsc()));
            editPsc.setVisibility(View.VISIBLE);
        }
        Button btnLocation = findViewById(R.id.buttonContractEditLocation);
        btnLocation.setOnClickListener(v -> {
            locationProviderClient = LocationServices.getFusedLocationProviderClient(this);
            getLocation();
            //mam adresu, chci vymenit za GPS, skryji adresni editTexty
            if (!Objects.isNull(contract.getAddress()) && !Objects.isNull(myLocation)) {
                editStreet.setVisibility(View.GONE);
                editHouseNum.setVisibility(View.GONE);
                editCity.setVisibility(View.GONE);
                editPsc.setVisibility(View.GONE);
            }
        });
        editNotes = findViewById(R.id.inputContractEditNotes);
        if (!Objects.isNull(contract.getNotes())) {
            editNotes.setText(contract.getNotes());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_toolbar_save:
                // User chose the "save" item
                if (TextUtils.isEmpty(editPrice.getText())) {
                    editPrice.setError("Nutno vyplnit!");
                } else if (Objects.isNull(myLocation) && Objects.isNull(contract.getLocation()) && TextUtils.isEmpty(editStreet.getText())) {
                    editStreet.setError("Nutno vyplnit!");
                } else if (Objects.isNull(myLocation) && Objects.isNull(contract.getLocation()) && TextUtils.isEmpty(editHouseNum.getText())) {
                    editHouseNum.setError("Nutno vyplnit!");
                } else if (Objects.isNull(myLocation) && Objects.isNull(contract.getLocation()) && TextUtils.isEmpty(editCity.getText())) {
                    editCity.setError("Nutno vyplnit!");
                } else if (Objects.isNull(myLocation) && Objects.isNull(contract.getLocation()) && TextUtils.isEmpty(editPsc.getText())) {
                    editPsc.setError("Nutno vyplnit!");
                } else {
                    //fotky
                    if (!Objects.isNull(photoBeforeUriNew)) {
                        File newFile = new File(photoBeforeUriNew);
                        if (newFile.length() == 0) {
                            newFile.delete();
                            photoBeforeUriNew = null;
                        } else {
                            //smaz stare foto
                            newFile = new File(photoBeforeUri);
                            newFile.delete();
                        }
                    }
                    if (!Objects.isNull(photoAfterUriNew)) {
                        File newFile = new File(photoAfterUriNew);
                        if (newFile.length() == 0) {
                            newFile.delete();
                            photoAfterUriNew = null;
                        } else {
                            newFile = new File(photoAfterUri);
                            newFile.delete();
                        }
                    }
                    String notes = editNotes.getText().toString();
                    BigDecimal price;
                    if (!editPrice.getText().toString().isEmpty()) {
                        price = new BigDecimal(editPrice.getText().toString());
                    } else {
                        price = BigDecimal.valueOf(0.0);
                    }
                    if (!Objects.isNull(client)) {
                        contract.setCustomerId(client.getId());
                    }
                    if (!Objects.isNull(inventoryItem)) {
                        contract.setItemId(inventoryItem.getId());
                    }
                    contract.setStartDate(dateOfContract);
                    contract.setExpEndDate(dateExpectedReturn);
                    if (!Objects.isNull(dateOfReturn)) {
                        contract.setEndDate(dateOfReturn);
                        contract.setReturned(true);
                    }
                    contract.setPrice(price);
                    if (!Objects.isNull(photoBeforeUriNew)) {
                        contract.setPhotoUriBefore(photoBeforeUriNew);
                    }
                    if (!Objects.isNull(photoAfterUriNew)) {
                        contract.setPhotoUriAfter(photoAfterUriNew);
                    }
                    //pokud mame novou GPS lokaci, ulozime ji a pripadne smazeme starou adresu
                    if (!Objects.isNull(myLocation)) {
                        contract.setLocation(myLocation);
                        if (!Objects.isNull(contract.getAddress())) {
                            contract.setAddress(null);
                        }
                    }
                    //mame jen adresu, ulozime adresu
                    if (Objects.isNull(myLocation) && Objects.isNull(contract.getLocation())) {
                        Address address = new Address(editStreet.getText().toString(), Integer.parseInt(editHouseNum.getText().toString()), editCity.getText().toString(), Integer.parseInt(editPsc.getText().toString()));
                        contract.setAddress(address);
                    }
                    //meli jsme GPS, chteli jsme zmenit, nepovoleno -> adresa
                    if (Objects.isNull(myLocation) && !Objects.isNull(contract.getLocation()) && !Objects.isNull(editStreet.getText()) && !Objects.isNull(editHouseNum.getText()) && !Objects.isNull(editCity.getText()) && !Objects.isNull(editPsc.getText())) {
                        contract.setLocation(null);
                        Address address = new Address(editStreet.getText().toString(), Integer.parseInt(editHouseNum.getText().toString()), editCity.getText().toString(), Integer.parseInt(editPsc.getText().toString()));
                        contract.setAddress(address);
                    }
                    contract.setNotes(notes);

                    Thread netThread = new Thread(() -> db.contractDao().update(contract));
                    netThread.start();
                    finish();
                    return true;
                }
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_toolbar_save, menu);
        return true;
    }

    private File createImageFile(int request) throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        if (request == REQUEST_IMAGE_CAPTURE_BEFORE) {
            photoBeforeUriNew = image.getAbsolutePath();
        } else if (request == REQUEST_IMAGE_CAPTURE_AFTER) {
            photoAfterUriNew = image.getAbsolutePath();
        }
        return image;
    }

    private void dispatchTakePictureIntent(int request) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile(request);
            } catch (IOException ex) {
                // Error occurred while creating the File
                Log.d(TAG, "An unrecoverable error occured while creating photo file");
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, request);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == LAUNCH_CLIENT_ACTIVITY) {
            if (resultCode == Activity.RESULT_OK) {
                client = (Client) data.getExtras().getSerializable("client");
                clientName.setText(client.getFirstName() + " " + client.getLastName());
                clientAddressStreetHouseNum.setText(client.getAddress().getStreetName() + " " + client.getAddress().getHouseNumber());
                clientAddressCityPSC.setText(client.getAddress().getCity() + " " + client.getAddress().getPsc());
            }
        }
        if (requestCode == LAUNCH_ITEM_ACTIVITY) {
            if (resultCode == Activity.RESULT_OK) {
                inventoryItem = (InventoryItem) data.getExtras().getSerializable("item");
                itemName.setText(inventoryItem.getName());
                itemSerialNum.setText(inventoryItem.getSerialNumber());
            }
        }
        if (requestCode == REQUEST_IMAGE_CAPTURE_BEFORE & resultCode == RESULT_OK) {
            setThumbnail(imgViewBefore, photoBeforeUriNew);
            Toast.makeText(this, "Foto uloženo", Toast.LENGTH_SHORT).show();
        }
        if (requestCode == REQUEST_IMAGE_CAPTURE_AFTER & resultCode == RESULT_OK) {
            setThumbnail(imgViewAfter, photoAfterUriNew);
            Toast.makeText(this, "Foto uloženo", Toast.LENGTH_SHORT).show();
        }
    }

    private void setThumbnail(ImageView imageView, String photoUri) {
        // Get the dimensions of the View
        int targetW = imageView.getLayoutParams().width;
        int targetH = imageView.getLayoutParams().height;
        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(photoUri, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;
        // Determine how much to scale down the image
        int scaleFactor = Math.max(1, Math.min(photoW / targetW, photoH / targetH));
        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        Bitmap bitmap = BitmapFactory.decodeFile(photoUri, bmOptions);
        runOnUiThread(() -> imageView.setImageBitmap(bitmap));
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, ACCESS_FINE_LOCATION_REQUEST_CODE);
            return;
        }
        locationProviderClient.getLastLocation()
                .addOnSuccessListener(this, location -> {
                    myLocation = new Location(location.getLatitude(), location.getLongitude());
                    editStreet.setVisibility(View.GONE);
                    editHouseNum.setVisibility(View.GONE);
                    editCity.setVisibility(View.GONE);
                    editPsc.setVisibility(View.GONE);
                    Toast.makeText(this, "GPS uloženo!", Toast.LENGTH_SHORT).show();
                })
                .addOnFailureListener(this, e -> Log.e("location", "location failed" + e.getMessage()));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case ACCESS_FINE_LOCATION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getLocation();
                } else {
                    //if permission denied
                    Toast.makeText(this, "Nelze změnit GPS!", Toast.LENGTH_SHORT).show();
                    editStreet.setVisibility(View.VISIBLE);
                    editHouseNum.setVisibility(View.VISIBLE);
                    editCity.setVisibility(View.VISIBLE);
                    editPsc.setVisibility(View.VISIBLE);
                }
        }
    }
}