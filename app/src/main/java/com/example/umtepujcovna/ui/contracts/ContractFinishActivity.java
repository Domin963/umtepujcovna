package com.example.umtepujcovna.ui.contracts;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;

import com.example.umtepujcovna.AppDatabase;
import com.example.umtepujcovna.R;
import com.example.umtepujcovna.objects.Contract;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class ContractFinishActivity extends AppCompatActivity {
    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final String TAG = ContractFinishActivity.class.getName();
    private final AppDatabase db = AppDatabase.getDatabase(this);
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    private Contract contract;
    private Toolbar myToolbar;
    private LocalDate finishDate;
    private String currentPhotoPath;
    private EditText editFinishDate, editNotes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contract_finish);
        myToolbar = findViewById(R.id.contractFinishToolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle("Vrácení výpůjčky");
        contract = (Contract) getIntent().getExtras().getSerializable("contractToFinish");

        editNotes = findViewById(R.id.inputContractFinishNotes);
        editNotes.setText(contract.getNotes());
        editFinishDate = findViewById(R.id.contractFinishDate);
        finishDate = LocalDate.now();

        DatePickerDialog.OnDateSetListener datePicker = (view, year, monthOfYear, dayOfMonth) -> {
            finishDate = finishDate.withYear(year).withMonth(monthOfYear + 1).withDayOfMonth(dayOfMonth);
            editFinishDate.setText(formatter.format(finishDate));
        };
        editFinishDate.setOnClickListener(v -> new DatePickerDialog(this, datePicker, finishDate.getYear(), finishDate.getMonth().getValue() - 1,
                finishDate.getDayOfMonth()).show());

        Button btnPhotoAfter = findViewById(R.id.btnContractFinishPhotoAfter);
        btnPhotoAfter.setOnClickListener(v -> dispatchTakePictureIntent());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_toolbar_save, menu);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE & resultCode == RESULT_OK) {
            Toast.makeText(this, "Foto uloženo", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_toolbar_save:
                // User chose the "save" item
                if (TextUtils.isEmpty(editFinishDate.getText())) {
                    editFinishDate.setError("Nutno vyplnit!");
                } else {
                    String notes = editNotes.getText().toString();
                    File file = new File(currentPhotoPath);
                    if (file.length() == 0) {
                        file.delete();
                        currentPhotoPath = null;
                    }
                    contract.setEndDate(finishDate);
                    contract.setPhotoUriAfter(currentPhotoPath);
                    contract.setNotes(notes);
                    contract.setReturned(true);
                    Thread netThread = new Thread(() -> db.contractDao().update(contract));
                    netThread.start();
                    finish();
                    return true;
                }
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Log.d(TAG, "An unrecoverable error occured while creating photo file");
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }
}