package com.example.umtepujcovna.ui.contracts;

import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.example.umtepujcovna.AppDatabase;
import com.example.umtepujcovna.R;
import com.example.umtepujcovna.objects.Contract;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class ContractsFragment extends Fragment {
    private AppDatabase db;
    private ListView listView;
    private CustomContractsListAdapter adapter;
    private List<Contract> contracts;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_contracts, container, false);
        db = AppDatabase.getDatabase(getActivity());
        Thread netThread = new Thread(() -> updateContracts());
        //pridani
        FloatingActionButton btnContractAdd = root.findViewById(R.id.btnContractAdd);
        btnContractAdd.setOnClickListener(v -> {
            Intent intent = new Intent(root.getContext(), ContractAddActivity.class);
            startActivity(intent);
        });
        contracts = new ArrayList<>();
        netThread.start();
        listView = root.findViewById(R.id.contractsListView);
        adapter = new CustomContractsListAdapter(root.getContext(), contracts);
        listView.setAdapter(adapter);
        registerForContextMenu(listView);
        //detail
        listView.setOnItemClickListener((parent, view, position, id) -> {
            Contract contract = (Contract) listView.getItemAtPosition(position);
            Intent intent = new Intent(root.getContext(), ContractDetailActivity.class);
            intent.putExtra("contract", contract);
            startActivity(intent);
        });
        return root;
    }

    public void onResume() {
        super.onResume();
        Thread netThread = new Thread(() -> {
            updateContracts();
            getActivity().runOnUiThread(() -> {
                adapter = new CustomContractsListAdapter(getContext(), contracts);
                listView.setAdapter(adapter);
            });
        });
        netThread.start();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (v.getId() == R.id.contractsListView) {
            MenuInflater inflater = getActivity().getMenuInflater();
            inflater.inflate(R.menu.menu_list, menu);
            MenuItem returnContract = menu.findItem(R.id.finish_contract);
            returnContract.setVisible(true);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.finish_contract:
                //return of loaned item
                Contract contractToFinish = (Contract) listView.getItemAtPosition(info.position);
                Intent intentFinish = new Intent(getActivity(), ContractFinishActivity.class);
                intentFinish.putExtra("contractToFinish", contractToFinish);
                startActivity(intentFinish);
                return true;
            case R.id.edit:
                // edit stuff here
                Contract contractToEdit = (Contract) listView.getItemAtPosition(info.position);
                Intent intent = new Intent(getActivity(), ContractEditActivity.class);
                intent.putExtra("contractToEdit", contractToEdit);
                startActivity(intent);
                return true;
            case R.id.delete:
                // remove stuff here
                Contract contractToDelete = (Contract) listView.getItemAtPosition(info.position);
                new AlertDialog.Builder(getActivity())
                        .setTitle("Upozornění")
                        .setMessage("Opravdu chcete výpůjčku smazat?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, (dialog, whichButton) -> {
                            Thread netThread = new Thread(() -> {
                                db.contractDao().delete(contractToDelete);
                                updateContracts();
                                getActivity().runOnUiThread(() -> {
                                    adapter = new CustomContractsListAdapter(getContext(), contracts);
                                    listView.setAdapter(adapter);
                                });
                            });
                            netThread.start();
                            Toast.makeText(getActivity(), "Smazáno", Toast.LENGTH_SHORT).show();
                        })
                        .setNegativeButton(android.R.string.no, null).show();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void updateContracts() {
        contracts = db.contractDao().getAll();
        for (int i = 0; i < contracts.size(); i++) {
            Contract c = contracts.get(i);
            c.setClient(db.clientDao().getOne(c.getCustomerId()));
            c.setItem(db.inventoryItemDao().getOne(c.getItemId()));
            contracts.set(i, c);
        }
    }
}