package com.example.umtepujcovna.ui.contracts;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import com.example.umtepujcovna.AppDatabase;
import com.example.umtepujcovna.R;
import com.example.umtepujcovna.objects.Address;
import com.example.umtepujcovna.objects.Client;
import com.example.umtepujcovna.objects.Contract;
import com.example.umtepujcovna.objects.InventoryItem;
import com.example.umtepujcovna.objects.Location;
import com.example.umtepujcovna.ui.clients.ClientListActivity;
import com.example.umtepujcovna.ui.inventory.InventoryListActivity;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Objects;

public class ContractAddActivity extends AppCompatActivity {
    private static final int REQUEST_IMAGE_CAPTURE = 3;
    private static final String TAG = ContractAddActivity.class.getName();
    private static final int ACCESS_FINE_LOCATION_REQUEST_CODE = 4;
    private static final int LAUNCH_CLIENT_ACTIVITY = 1;
    private static final int LAUNCH_ITEM_ACTIVITY = 2;
    private Toolbar myToolbar;
    private AppDatabase db = AppDatabase.getDatabase(this);
    private TextView clientName, clientAddressStreetHouseNum, clientAddressCityPSC;
    private TextView itemName, itemSerialNum;
    private EditText editDateOfContract, editDateExpectedReturn;
    private EditText textLocationStreet, textLocationHouseNum, textLocationCity, textLocationPsc;
    private LocalDate dateOfContract, dateExpectedReturn;
    private Client client;
    private InventoryItem inventoryItem;
    private String currentPhotoPath;
    private Location myLocation;
    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    private FusedLocationProviderClient locationProviderClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contract_add);
        myToolbar = findViewById(R.id.contractAddToolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle("Nová výpůjčka");

        Button btnCustomer = findViewById(R.id.buttonSelectClient);
        btnCustomer.setOnClickListener(v -> {
            Intent i = new Intent(this, ClientListActivity.class);
            startActivityForResult(i, LAUNCH_CLIENT_ACTIVITY);
        });
        clientName = findViewById(R.id.contractAddClientName);
        clientAddressStreetHouseNum = findViewById(R.id.contractAddClientAddressStreetHouseNum);
        clientAddressCityPSC = findViewById(R.id.contractAddClientAddressCityPSC);

        Button btnInventoryItem = findViewById(R.id.buttonSelectInventoryItem);
        btnInventoryItem.setOnClickListener(v -> {
            Intent i = new Intent(this, InventoryListActivity.class);
            startActivityForResult(i, LAUNCH_ITEM_ACTIVITY);
        });
        itemName = findViewById(R.id.contractAddItemName);
        itemSerialNum = findViewById(R.id.contractAddItemSerialNum);
        DatePickerDialog.OnDateSetListener datePicker1 = (view, year, monthOfYear, dayOfMonth) -> {
            dateOfContract = dateOfContract.withYear(year).withMonth(monthOfYear + 1).withDayOfMonth(dayOfMonth);
            editDateOfContract.setText(formatter.format(dateOfContract));
        };
        DatePickerDialog.OnDateSetListener datePicker2 = (view, year, monthOfYear, dayOfMonth) -> {
            dateExpectedReturn = dateExpectedReturn.withYear(year).withMonth(monthOfYear + 1).withDayOfMonth(dayOfMonth);
            editDateExpectedReturn.setText(formatter.format(dateExpectedReturn));
        };
        dateOfContract = LocalDate.now();
        editDateOfContract = findViewById(R.id.inputContractAddDateOfContract);
        editDateOfContract.setOnClickListener(v -> new DatePickerDialog(this, datePicker1, dateOfContract.getYear(), dateOfContract.getMonth().getValue() - 1,
                dateOfContract.getDayOfMonth()).show());
        dateExpectedReturn = LocalDate.now();
        editDateExpectedReturn = findViewById(R.id.inputContractAddDateOfExpectedReturn);
        editDateExpectedReturn.setOnClickListener(v -> new DatePickerDialog(this, datePicker2, dateExpectedReturn.getYear(), dateExpectedReturn.getMonth().getValue() - 1,
                dateExpectedReturn.getDayOfMonth()).show());

        //porizeni fotografie
        Button btnPhoto = findViewById(R.id.buttonContractAddPhotoBefore);
        btnPhoto.setOnClickListener(v -> dispatchTakePictureIntent());

        locationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        getLocation();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_toolbar_save:
                // User chose the "save" item
                EditText textPrice = findViewById(R.id.inputContractAddPrice);
                EditText textNotes = findViewById(R.id.inputContractAddNotes);
                if (TextUtils.isEmpty(clientAddressStreetHouseNum.getText())) {
                    clientAddressStreetHouseNum.setError("Nutno vyplnit!");
                } else if (TextUtils.isEmpty(itemName.getText())) {
                    itemName.setError("Nutno vyplnit!");
                } else if (TextUtils.isEmpty(editDateOfContract.getText())) {
                    editDateOfContract.setError("Nutno vyplnit!");
                } else if (TextUtils.isEmpty(editDateExpectedReturn.getText())) {
                    editDateExpectedReturn.setError("Nutno vyplnit!");
                } else if (TextUtils.isEmpty(textPrice.getText())) {
                    textPrice.setError("Nutno vyplnit!");
                } else if (TextUtils.isEmpty(clientName.getText())) {
                    clientName.setError("Nutno vyplnit!");
                } else if (Objects.isNull(myLocation) && TextUtils.isEmpty(textLocationStreet.getText())) {
                    textLocationStreet.setError("Nutno vyplnit!");
                } else if (Objects.isNull(myLocation) && TextUtils.isEmpty(textLocationHouseNum.getText())) {
                    textLocationHouseNum.setError("Nutno vyplnit!");
                } else if (Objects.isNull(myLocation) && TextUtils.isEmpty(textLocationCity.getText())) {
                    textLocationCity.setError("Nutno vyplnit!");
                } else if (Objects.isNull(myLocation) && TextUtils.isEmpty(textLocationPsc.getText())) {
                    textLocationPsc.setError("Nutno vyplnit!");
                } else {
                    String notes = textNotes.getText().toString();
                    BigDecimal price;
                    if (!textPrice.getText().toString().isEmpty()) {
                        price = new BigDecimal(textPrice.getText().toString());
                    } else {
                        price = BigDecimal.valueOf(0.0);
                    }
                    File file = new File(currentPhotoPath);
                    if (file.length() == 0) {
                        file.delete();
                        currentPhotoPath = null;
                    }
                    Contract contract = new Contract(dateOfContract, dateExpectedReturn, null, client.getId(), price, false, inventoryItem.getId(), currentPhotoPath, null, notes);
                    if (Objects.isNull(myLocation)) {
                        Address locationAddress = new Address(textLocationStreet.getText().toString(),
                                Integer.parseInt(textLocationHouseNum.getText().toString()),
                                textLocationCity.getText().toString(),
                                Integer.parseInt(textLocationPsc.getText().toString()));
                        contract.setAddress(locationAddress);
                    } else {
                        contract.setLocation(myLocation);
                    }
                    Thread netThread = new Thread(() -> db.contractDao().insert(contract));
                    netThread.start();
                    finish();
                    return true;
                }
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == LAUNCH_CLIENT_ACTIVITY) {
            if (resultCode == Activity.RESULT_OK) {
                client = (Client) data.getExtras().getSerializable("client");

                clientName.setText(client.getFirstName() + " " + client.getLastName());
                clientAddressStreetHouseNum.setText(client.getAddress().getStreetName() + " " + client.getAddress().getHouseNumber());
                clientAddressCityPSC.setText(client.getAddress().getCity() + " " + client.getAddress().getPsc());
            }
        }
        if (requestCode == LAUNCH_ITEM_ACTIVITY) {
            if (resultCode == Activity.RESULT_OK) {
                inventoryItem = (InventoryItem) data.getExtras().getSerializable("item");

                itemName.setText(inventoryItem.getName());
                itemSerialNum.setText(inventoryItem.getSerialNumber());
            }
        }
        if (requestCode == REQUEST_IMAGE_CAPTURE & resultCode == RESULT_OK) {
            Toast.makeText(this, "Foto uloženo", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_toolbar_save, menu);
        return true;
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Log.d(TAG, "An unrecoverable error occured while creating photo file");
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, ACCESS_FINE_LOCATION_REQUEST_CODE);
            return;
        }
        locationProviderClient.getLastLocation()
                .addOnSuccessListener(this, location -> myLocation = new Location(location.getLatitude(), location.getLongitude()))
                .addOnFailureListener(this, e -> Log.e("location", "location failed" + e.getMessage()));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case ACCESS_FINE_LOCATION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getLocation();
                } else {
                    //if permission denied, show EditTexts to manually write location
                    TextView textLocationLabel = findViewById(R.id.contractAddLocationLabel);
                    textLocationStreet = findViewById(R.id.inputContractAddLocationStreet);
                    textLocationHouseNum = findViewById(R.id.inputContractAddLocationHouseNum);
                    textLocationCity = findViewById(R.id.inputContractAddLocationCity);
                    textLocationPsc = findViewById(R.id.inputContractAddLocationPsc);
                    textLocationLabel.setVisibility(View.VISIBLE);
                    textLocationStreet.setVisibility(View.VISIBLE);
                    textLocationHouseNum.setVisibility(View.VISIBLE);
                    textLocationCity.setVisibility(View.VISIBLE);
                    textLocationPsc.setVisibility(View.VISIBLE);
                }
        }
    }
}
