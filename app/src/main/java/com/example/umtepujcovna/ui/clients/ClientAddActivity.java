package com.example.umtepujcovna.ui.clients;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.umtepujcovna.AppDatabase;
import com.example.umtepujcovna.R;
import com.example.umtepujcovna.objects.Address;
import com.example.umtepujcovna.objects.Client;

public class ClientAddActivity extends AppCompatActivity {
    private Toolbar myToolbar;
    private AppDatabase db = AppDatabase.getDatabase(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_add);
        myToolbar = findViewById(R.id.clientAddToolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle("Nový zákazník");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_toolbar_save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_toolbar_save:
                // User chose the "save" item
                EditText textFirstName = findViewById(R.id.inputClientFirstName);
                EditText textLastName = findViewById(R.id.inputClientLastName);
                EditText textPhone = findViewById(R.id.inputClientPhone);
                EditText textIco = findViewById(R.id.inputClientIco);
                EditText textAddressStreet = findViewById(R.id.inputClientAddressStreet);
                EditText textAddressHouseNum = findViewById(R.id.inputClientAddressHouseNumber);
                EditText textAddressCity = findViewById(R.id.inputClientAddressCity);
                EditText textAddressPsc = findViewById(R.id.inputClientAddressPsc);
                //validace
                if (TextUtils.isEmpty(textFirstName.getText())) {
                    textFirstName.setError("Nutno vyplnit!");
                } else if (TextUtils.isEmpty(textLastName.getText())) {
                    textLastName.setError("Nutno vyplnit!");
                } else if (TextUtils.isEmpty(textPhone.getText())) {
                    textPhone.setError("Nutno vyplnit!");
                } else if (TextUtils.isEmpty(textAddressStreet.getText())) {
                    textAddressStreet.setError("Nutno vyplnit!");
                } else if (TextUtils.isEmpty(textAddressHouseNum.getText())) {
                    textAddressHouseNum.setError("Nutno vyplnit!");
                } else if (TextUtils.isEmpty(textAddressCity.getText())) {
                    textAddressCity.setError("Nutno vyplnit!");
                } else if (TextUtils.isEmpty(textAddressPsc.getText())) {
                    textAddressPsc.setError("Nutno vyplnit!");
                } else {

                    String firstName = textFirstName.getText().toString();
                    String lastName = textLastName.getText().toString();
                    int phone = Integer.parseInt(textPhone.getText().toString());
                    int ico = 0;
                    if (!TextUtils.isEmpty(textIco.getText())) {
                        ico = Integer.parseInt(textIco.getText().toString());
                    }
                    String addressStreet = textAddressStreet.getText().toString();
                    int addressHouseNum = Integer.parseInt(textAddressHouseNum.getText().toString());
                    String addressCity = textAddressCity.getText().toString();
                    int addressPsc = Integer.parseInt(textAddressPsc.getText().toString());

                    Address address = new Address(addressStreet, addressHouseNum, addressCity, addressPsc);
                    Client client = new Client(firstName, lastName, address, phone, ico);

                    Thread netThread = new Thread(() -> db.clientDao().insert(client));
                    netThread.start();
                    finish();
                    return true;
                }
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }
}