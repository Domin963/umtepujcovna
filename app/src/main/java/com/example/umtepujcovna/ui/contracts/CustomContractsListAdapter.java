package com.example.umtepujcovna.ui.contracts;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.umtepujcovna.R;
import com.example.umtepujcovna.objects.Contract;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;

public class CustomContractsListAdapter extends BaseAdapter {
    private List<Contract> contractList;
    private LayoutInflater layoutInflater;

    public CustomContractsListAdapter(Context aContext, List<Contract> contractList) {
        this.contractList = contractList;
        this.layoutInflater = LayoutInflater.from(aContext);
    }

    @Override
    public int getCount() {
        return contractList.size();
    }

    @Override
    public Object getItem(int position) {
        return contractList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View v, ViewGroup vg) {
        ViewHolder holder;
        if (v == null) {
            v = layoutInflater.inflate(R.layout.contracts_list_row, null);
            holder = new ViewHolder();
            holder.uName = v.findViewById(R.id.contractClientName);
            holder.uItem = v.findViewById(R.id.contractItem);
            holder.uDate = v.findViewById(R.id.contractStartDate);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }
        holder.uName.setText(contractList.get(position).getClient().getFirstName() + " " + contractList.get(position).getClient().getLastName());
        holder.uItem.setText(contractList.get(position).getItem().getName());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        if(!Objects.isNull(contractList.get(position).getEndDate())) {
            holder.uDate.setText(formatter.format(contractList.get(position).getStartDate())+" - "+formatter.format(contractList.get(position).getEndDate()));
        } else {
            holder.uDate.setText(formatter.format(contractList.get(position).getStartDate()));
        }
        return v;
    }

    static class ViewHolder {
        TextView uName;
        TextView uItem;
        TextView uDate;
    }
}
