package com.example.umtepujcovna.ui.inventory;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;

import com.example.umtepujcovna.AppDatabase;
import com.example.umtepujcovna.R;
import com.example.umtepujcovna.objects.InventoryItem;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class InventoryEditActivity extends AppCompatActivity {
    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final String TAG = InventoryAddActivity.class.getName();
    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    private String currentPhotoPath;
    private Toolbar myToolbar;
    private LocalDate localDate;
    private InventoryItem item;
    private EditText editName, editSerial, editPrice, editDate;
    private AppDatabase db = AppDatabase.getDatabase(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventory_edit);
        myToolbar = findViewById(R.id.inventoryEditToolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle("Editace");

        item = (InventoryItem) getIntent().getExtras().getSerializable("itemToEdit");
        currentPhotoPath = item.getPhotoURI();
        editName = findViewById(R.id.inventoryEditName);
        editName.setText(item.getName());
        editSerial = findViewById(R.id.inventoryEditSerialNumber);
        editSerial.setText(item.getSerialNumber());
        editDate = findViewById(R.id.inventoryEditDateOfPurchase);
        editDate.setText(formatter.format(item.getDateOfPurchase()));
        editPrice = findViewById(R.id.inventoryEditPrice);
        editPrice.setText(item.getPrice().toString());
        localDate = item.getDateOfPurchase();
        DatePickerDialog.OnDateSetListener datePicker = (view, year, monthOfYear, dayOfMonth) -> {
            localDate = localDate.withYear(year);
            localDate = localDate.withMonth(monthOfYear + 1);
            localDate = localDate.withDayOfMonth(dayOfMonth);
            editDate.setText(formatter.format(localDate));
        };

        editDate.setOnClickListener(v -> new DatePickerDialog(InventoryEditActivity.this, datePicker, localDate.getYear(), localDate.getMonth().getValue() - 1,
                localDate.getDayOfMonth()).show());
        ImageView imageView = findViewById(R.id.inventoryEditImgView);
        if (item.getPhotoURI() != null) {
            Bitmap bitmap = BitmapFactory.decodeFile(item.getPhotoURI());
            imageView.setImageBitmap(bitmap);
        } else {
            Log.d(TAG, "PHOTO NOT FOUND");
        }
        //porizeni fotografie
        Button btnPhoto = findViewById(R.id.btnInventoryEditPhoto);
        btnPhoto.setOnClickListener(v -> dispatchTakePictureIntent());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_toolbar_save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.action_toolbar_save:
                // User chose the "save" item

                String name = editName.getText().toString();
                String serial = editSerial.getText().toString();
                BigDecimal price;
                if (!editPrice.getText().toString().isEmpty()) {
                    price = new BigDecimal(editPrice.getText().toString());
                } else {
                    price = item.getPrice();
                }
                LocalDate date = LocalDate.parse(editDate.getText(), DateTimeFormatter.ofPattern("dd.MM.yyyy"));
                item.setName(name);
                item.setSerialNumber(serial);
                item.setPrice(price);
                item.setDateOfPurchase(date);
                if (new File(currentPhotoPath).length() == 0) {
                    File fdelete = new File(currentPhotoPath);
                    fdelete.delete();
                    currentPhotoPath = item.getPhotoURI();
                }
                //pri nahrazeni fotografie starou smaz
                if (!currentPhotoPath.equals(item.getPhotoURI())) {
                    File fdelete = new File(item.getPhotoURI());
                    fdelete.delete();
                }
                item.setPhotoURI(currentPhotoPath);
                Thread netThread = new Thread(() -> db.inventoryItemDao().update(item));
                netThread.start();
                finish();
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(menuItem);
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Log.d(TAG, "An unrecoverable error occured while creating photo file");
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE & resultCode == RESULT_OK) {
            ImageView imageView = findViewById(R.id.inventoryEditImgView);
            // Get the dimensions of the View
            int targetW = imageView.getWidth();
            int targetH = imageView.getHeight();
            // Get the dimensions of the bitmap
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(currentPhotoPath, bmOptions);
            int photoW = bmOptions.outWidth;
            int photoH = bmOptions.outHeight;
            // Determine how much to scale down the image
            int scaleFactor = Math.max(1, Math.min(photoW / targetW, photoH / targetH));
            // Decode the image file into a Bitmap sized to fill the View
            bmOptions.inJustDecodeBounds = false;
            bmOptions.inSampleSize = scaleFactor;
            Bitmap bitmap = BitmapFactory.decodeFile(currentPhotoPath, bmOptions);
            imageView.setImageBitmap(bitmap);
        } else {
            Log.d(TAG, "PROBLEM WITH PHOTO");
        }
    }
}