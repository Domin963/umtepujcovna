package com.example.umtepujcovna.ui.inventory;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.umtepujcovna.R;
import com.example.umtepujcovna.objects.InventoryItem;

import java.time.format.DateTimeFormatter;

public class InventoryDetailActivity extends AppCompatActivity {
    private static final String TAG = InventoryDetailActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventory_detail);
        assert getSupportActionBar() != null;   //null check
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);   //show back button
        InventoryItem item = (InventoryItem) getIntent().getExtras().getSerializable("item");

        TextView name = findViewById(R.id.inventoryDetailName);
        name.setText(item.getName());
        TextView price = findViewById(R.id.inventoryDetailPrice);
        price.setText(item.getPrice().toString() + " Kč");
        TextView dateOfPurchase = findViewById(R.id.inventoryDetailDateOfPurchase);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        dateOfPurchase.setText(formatter.format(item.getDateOfPurchase()));
        TextView serial = findViewById(R.id.inventoryDetailSerialNum);
        serial.setText(item.getSerialNumber());

        ImageView imageView = findViewById(R.id.inventoryDetailImgView);
        if (item.getPhotoURI() != null) {
            Bitmap bitmap = BitmapFactory.decodeFile(item.getPhotoURI());
            imageView.setImageBitmap(bitmap);
        } else {
            Log.d(TAG, "PHOTO NOT FOUND");
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}