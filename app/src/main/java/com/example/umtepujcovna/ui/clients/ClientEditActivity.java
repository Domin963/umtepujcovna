package com.example.umtepujcovna.ui.clients;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.umtepujcovna.AppDatabase;
import com.example.umtepujcovna.R;
import com.example.umtepujcovna.objects.Client;

public class ClientEditActivity extends AppCompatActivity {
    private Toolbar myToolbar;
    private AppDatabase db = AppDatabase.getDatabase(this);
    private Client client;
    private EditText textFirstName, textLastName, textPhone, textIco, textAddressStreet, textAddressHouseNum, textAddressCity, textAddressPsc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_edit);
        myToolbar = findViewById(R.id.clientEditToolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle("Editace");

        client = (Client) getIntent().getExtras().getSerializable("clientToEdit");
        textFirstName = findViewById(R.id.clientEditFirstName);
        textLastName = findViewById(R.id.clientEditLastName);
        textPhone = findViewById(R.id.clientEditPhoneNum);
        textIco = findViewById(R.id.clientEditIco);
        textAddressStreet = findViewById(R.id.clientEditStreetName);
        textAddressHouseNum = findViewById(R.id.clientEditHouseNum);
        textAddressCity = findViewById(R.id.clientEditCity);
        textAddressPsc = findViewById(R.id.clientEditPsc);

        textFirstName.setText(client.getFirstName());
        textLastName.setText(client.getLastName());
        textPhone.setText(String.valueOf(client.getPhoneNumber()));
        textIco.setText(String.valueOf(client.getIco()));
        textAddressStreet.setText(client.getAddress().getStreetName());
        textAddressHouseNum.setText(String.valueOf(client.getAddress().getHouseNumber()));
        textAddressCity.setText(client.getAddress().getCity());
        textAddressPsc.setText(String.valueOf(client.getAddress().getPsc()));


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_toolbar_save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_toolbar_save:
                // User chose the "save" item

                //validace
                if (TextUtils.isEmpty(textFirstName.getText())) {
                    textFirstName.setError("Nutno vyplnit!");
                } else if (TextUtils.isEmpty(textLastName.getText())) {
                    textLastName.setError("Nutno vyplnit!");
                } else if (TextUtils.isEmpty(textPhone.getText())) {
                    textPhone.setError("Nutno vyplnit!");
                } else if (TextUtils.isEmpty(textAddressStreet.getText())) {
                    textAddressStreet.setError("Nutno vyplnit!");
                } else if (TextUtils.isEmpty(textAddressHouseNum.getText())) {
                    textAddressHouseNum.setError("Nutno vyplnit!");
                } else if (TextUtils.isEmpty(textAddressCity.getText())) {
                    textAddressCity.setError("Nutno vyplnit!");
                } else if (TextUtils.isEmpty(textAddressPsc.getText())) {
                    textAddressPsc.setError("Nutno vyplnit!");
                } else {

                    client.setFirstName(textFirstName.getText().toString());
                    client.setLastName(textLastName.getText().toString());
                    client.setPhoneNumber(Integer.parseInt(textPhone.getText().toString()));
                    client.setIco(!TextUtils.isEmpty(textIco.getText()) ? Integer.parseInt(textIco.getText().toString()) : 0);
                    client.getAddress().setStreetName(textAddressStreet.getText().toString());
                    client.getAddress().setHouseNumber(Integer.parseInt(textAddressHouseNum.getText().toString()));
                    client.getAddress().setCity(textAddressCity.getText().toString());
                    client.getAddress().setPsc(Integer.parseInt(textAddressPsc.getText().toString()));
                    Thread netThread = new Thread(() -> {
                        db.clientDao().update(client);
                    });
                    netThread.start();
                    finish();
                    return true;
                }
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }
}