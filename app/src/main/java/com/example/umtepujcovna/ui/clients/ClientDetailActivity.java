package com.example.umtepujcovna.ui.clients;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.umtepujcovna.R;
import com.example.umtepujcovna.objects.Client;

import org.w3c.dom.Text;

public class ClientDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_detail);
        assert getSupportActionBar() != null;   //null check
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);   //show back button
        Client client = (Client) getIntent().getExtras().getSerializable("client");

        TextView firstName = findViewById(R.id.clientDetailFirstName);
        firstName.setText(client.getFirstName());
        TextView lastName = findViewById(R.id.clientDetailLastName);
        lastName.setText(client.getLastName());
        TextView phoneNum = findViewById(R.id.clientDetailPhoneNum);
        phoneNum.setText(String.valueOf(client.getPhoneNumber()));
        TextView ico = findViewById(R.id.clientDetailIco);
        ico.setText(String.valueOf(client.getIco()));

        TextView street = findViewById(R.id.clientDetailStreetName);
        street.setText(client.getAddress().getStreetName());
        TextView houseNum = findViewById(R.id.clientDetailHouseNum);
        houseNum.setText(String.valueOf(client.getAddress().getHouseNumber()));
        TextView city = findViewById(R.id.clientDetailCity);
        city.setText(client.getAddress().getCity());
        TextView psc = findViewById(R.id.clientDetailPsc);
        psc.setText(String.valueOf(client.getAddress().getPsc()));
    }
    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}