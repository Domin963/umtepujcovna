package com.example.umtepujcovna.ui.contracts;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.umtepujcovna.R;
import com.example.umtepujcovna.objects.Contract;
import com.example.umtepujcovna.objects.Location;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ContractDetailActivity extends AppCompatActivity implements OnMapReadyCallback {
    private static final String TAG = ContractDetailActivity.class.getName();
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    private MapView mapView;
    private Geocoder geocoder;
    private Location myLocation;
    private Contract contract;
    private List<Address> addressList;
    private List<Address> addressListFromLocation;
    private TextView textLocationStreet;
    private TextView textLocationCity, textMapLabel;
    private Animator currentAnimator;
    private int shortAnimationDuration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contract_detail);
        assert getSupportActionBar() != null;   //null check
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);   //show back button
        getSupportActionBar().setTitle("Detail výpůjčky");
        contract = (Contract) getIntent().getExtras().getSerializable("contract");
        geocoder = new Geocoder(this);
        mapView = findViewById(R.id.contractDetailMapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        if (!Objects.isNull(contract.getLocation())) {
            myLocation = contract.getLocation();
            mapView.setVisibility(View.VISIBLE);
        }
        shortAnimationDuration = getResources().getInteger(
                android.R.integer.config_shortAnimTime);
        TextView textClientName = findViewById(R.id.contractDetailClientName);
        TextView textClientAddressStreet = findViewById(R.id.contractDetailClientAddressStreetHouseNum);
        TextView textClientAddressCity = findViewById(R.id.contractDetailClientAddressCityPsc);
        TextView textItemName = findViewById(R.id.contractDetailItemName);
        TextView textItemSerial = findViewById(R.id.contractDetailItemSerial);
        TextView textDateOfContract = findViewById(R.id.contractDetailDateofContract);
        TextView textDateOfExpRet = findViewById(R.id.contractDetailDateofExpectedReturn);
        TextView textDateOfRet = findViewById(R.id.contractDetailDateofReturn);
        TextView textPrice = findViewById(R.id.contractDetailPrice);
        TextView textNotesLabel = findViewById(R.id.contractDetailNotesLabel);
        TextView textNotes = findViewById(R.id.contractDetailNotes);
        TextView textLocationLabel = findViewById(R.id.contractDetailLocationLabel);
        textLocationStreet = findViewById(R.id.contractDetailLocationStreetHouseNum);
        textLocationCity = findViewById(R.id.contractDetailLocationCityPsc);
        textMapLabel = findViewById(R.id.contractDetailLocationMapLabel);
        ImageView imgBefore = findViewById(R.id.contractDetailImgBefore);
        ImageView imgAfter = findViewById(R.id.contractDetailImgAfter);

        textClientName.setText(contract.getClient().getFirstName() + " " + contract.getClient().getLastName());
        textClientAddressStreet.setText(contract.getClient().getAddress().getStreetName() + " " + contract.getClient().getAddress().getHouseNumber());
        textClientAddressCity.setText(contract.getClient().getAddress().getCity() + " " + contract.getClient().getAddress().getPsc());
        textItemName.setText(contract.getItem().getName());
        textItemSerial.setText(contract.getItem().getSerialNumber());
        textDateOfContract.setText(formatter.format(contract.getStartDate()));
        textDateOfExpRet.setText(formatter.format(contract.getExpEndDate()));
        if (!Objects.isNull(contract.getEndDate())) {
            TextView textDateOfRetLabel = findViewById(R.id.contractDetailDateOfReturnLabel);
            textDateOfRetLabel.setVisibility(View.VISIBLE);
            textDateOfRet.setVisibility(View.VISIBLE);
            textDateOfRet.setText(formatter.format(contract.getEndDate()));
        }
        textPrice.setText(contract.getPrice().toString() + " Kč");

        if (contract.getNotes() != "") {
            textNotesLabel.setVisibility(View.VISIBLE);
            textNotes.setVisibility(View.VISIBLE);
            textNotes.setText(contract.getNotes());
        }

        if (!Objects.isNull(myLocation)) {
            addressList = new ArrayList<>();
            Thread geoThread = new Thread(() -> {
                try {
                    addressList = geocoder.getFromLocation(myLocation.getLatitude(), myLocation.getLongitude(), 1);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (addressList.size() != 0) {
                    textLocationStreet.setText(addressList.get(0).getThoroughfare() + " " + addressList.get(0).getSubThoroughfare());
                    textLocationCity.setText(addressList.get(0).getLocality() + " " + addressList.get(0).getPostalCode());
                }
            });
            geoThread.start();

        } else if (!Objects.isNull(contract.getAddress())) {
            textLocationStreet.setText(contract.getAddress().getStreetName() + " " + contract.getAddress().getHouseNumber());
            textLocationCity.setText(contract.getAddress().getCity() + " " + contract.getAddress().getPsc());
        } else {
            textLocationLabel.setVisibility(View.GONE);
            textLocationStreet.setVisibility(View.GONE);
            textLocationCity.setVisibility(View.GONE);
        }

        if (contract.getPhotoUriBefore() != null) {
            TextView imgBeforeLabel = findViewById(R.id.contractDetailImgLabel);
            imgBeforeLabel.setVisibility(View.VISIBLE);
            imgBefore.setVisibility(View.VISIBLE);
            Thread loadImgBefore = new Thread(() -> setThumbnail(imgBefore, contract.getPhotoUriBefore()));
            loadImgBefore.start();
            //zoom on photo
            imgBefore.setOnClickListener(v -> zoomImageFromThumb(imgBefore, contract.getPhotoUriBefore()));
        } else {
            Log.d(TAG, "PHOTO NOT FOUND");
        }
        if (contract.getPhotoUriAfter() != null) {
            imgAfter.setVisibility(View.VISIBLE);
            Thread loadImgAfter = new Thread(() -> setThumbnail(imgAfter, contract.getPhotoUriAfter()));
            loadImgAfter.start();
            //zoom on photo
            imgAfter.setOnClickListener(v -> zoomImageFromThumb(imgAfter, contract.getPhotoUriAfter()));
        } else {
            Log.d(TAG, "PHOTO NOT FOUND");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onMapReady(GoogleMap map) {
        map.getUiSettings().setZoomControlsEnabled(true);
        if (!Objects.isNull(myLocation)) {
            map.addMarker(new MarkerOptions().position(new LatLng(myLocation.getLatitude(), myLocation.getLongitude())).title("Pozice"));
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()), 17f));
        } else if (!Objects.isNull(contract.getAddress())) {
            addressListFromLocation = new ArrayList<>();
            Thread geoThread2 = new Thread(() -> {
                try {
                    addressListFromLocation = geocoder.getFromLocationName(contract.getAddress().getStreetName() + " " + contract.getAddress().getHouseNumber() + ", " + contract.getAddress().getCity(), 1);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            geoThread2.start();
            try {
                geoThread2.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (addressListFromLocation.size() != 0) {
                Address add = addressListFromLocation.get(0);
                map.addMarker(new MarkerOptions().position(new LatLng(add.getLatitude(), add.getLongitude())).title("Pozice"));
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(add.getLatitude(), add.getLongitude()), 17f));
                textMapLabel.setVisibility(View.VISIBLE);
                mapView.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    protected void onPause() {
        mapView.onPause();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    private void setThumbnail(ImageView imageView, String photoUri) {
        // Get the dimensions of the View
        int targetW = imageView.getLayoutParams().width;
        int targetH = imageView.getLayoutParams().height;
        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(photoUri, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;
        // Determine how much to scale down the image
        int scaleFactor = Math.max(1, Math.min(photoW / targetW, photoH / targetH));
        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        Bitmap bitmap = BitmapFactory.decodeFile(photoUri, bmOptions);
        runOnUiThread(() -> imageView.setImageBitmap(bitmap));
    }

    private void zoomImageFromThumb(View thumbView, String photoUri) {
        // If there's an animation in progress, cancel it
        // immediately and proceed with this one.
        if (currentAnimator != null) {
            currentAnimator.cancel();
        }
        Bitmap bitmap = BitmapFactory.decodeFile(photoUri);
        // Load the high-resolution "zoomed-in" image.
        final ImageView expandedImageView = (ImageView) findViewById(
                R.id.contractDetailExpandedImage);
        expandedImageView.setImageBitmap(bitmap);
        // Calculate the starting and ending bounds for the zoomed-in image.
        // This step involves lots of math. Yay, math.
        final Rect startBounds = new Rect();
        final Rect finalBounds = new Rect();
        final Point globalOffset = new Point();
        // The start bounds are the global visible rectangle of the thumbnail,
        // and the final bounds are the global visible rectangle of the container
        // view. Also set the container view's offset as the origin for the
        // bounds, since that's the origin for the positioning animation
        // properties (X, Y).
        thumbView.getGlobalVisibleRect(startBounds);
        findViewById(R.id.contractDetailContainer)
                .getGlobalVisibleRect(finalBounds, globalOffset);
        startBounds.offset(-globalOffset.x, -globalOffset.y);
        finalBounds.offset(-globalOffset.x, -globalOffset.y);
        // Adjust the start bounds to be the same aspect ratio as the final
        // bounds using the "center crop" technique. This prevents undesirable
        // stretching during the animation. Also calculate the start scaling
        // factor (the end scaling factor is always 1.0).
        float startScale;
        if ((float) finalBounds.width() / finalBounds.height()
                > (float) startBounds.width() / startBounds.height()) {
            // Extend start bounds horizontally
            startScale = (float) startBounds.height() / finalBounds.height();
            float startWidth = startScale * finalBounds.width();
            float deltaWidth = (startWidth - startBounds.width()) / 2;
            startBounds.left -= deltaWidth;
            startBounds.right += deltaWidth;
        } else {
            // Extend start bounds vertically
            startScale = (float) startBounds.width() / finalBounds.width();
            float startHeight = startScale * finalBounds.height();
            float deltaHeight = (startHeight - startBounds.height()) / 2;
            startBounds.top -= deltaHeight;
            startBounds.bottom += deltaHeight;
        }
        // Hide the thumbnail and show the zoomed-in view. When the animation
        // begins, it will position the zoomed-in view in the place of the
        // thumbnail.
        thumbView.setAlpha(0f);
        expandedImageView.setVisibility(View.VISIBLE);
        // Set the pivot point for SCALE_X and SCALE_Y transformations
        // to the top-left corner of the zoomed-in view (the default
        // is the center of the view).
        expandedImageView.setPivotX(0f);
        expandedImageView.setPivotY(0f);
        // Construct and run the parallel animation of the four translation and
        // scale properties (X, Y, SCALE_X, and SCALE_Y).
        AnimatorSet set = new AnimatorSet();
        set
                .play(ObjectAnimator.ofFloat(expandedImageView, View.X,
                        startBounds.left, finalBounds.left))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.Y,
                        startBounds.top, finalBounds.top))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X,
                        startScale, 1f))
                .with(ObjectAnimator.ofFloat(expandedImageView,
                        View.SCALE_Y, startScale, 1f));
        set.setDuration(shortAnimationDuration);
        set.setInterpolator(new DecelerateInterpolator());
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                currentAnimator = null;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                currentAnimator = null;
            }
        });
        set.start();
        currentAnimator = set;

        // Upon clicking the zoomed-in image, it should zoom back down
        // to the original bounds and show the thumbnail instead of
        // the expanded image.
        final float startScaleFinal = startScale;
        expandedImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentAnimator != null) {
                    currentAnimator.cancel();
                }
                // Animate the four positioning/sizing properties in parallel,
                // back to their original values.
                AnimatorSet set = new AnimatorSet();
                set.play(ObjectAnimator
                        .ofFloat(expandedImageView, View.X, startBounds.left))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView,
                                        View.Y, startBounds.top))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView,
                                        View.SCALE_X, startScaleFinal))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView,
                                        View.SCALE_Y, startScaleFinal));
                set.setDuration(shortAnimationDuration);
                set.setInterpolator(new DecelerateInterpolator());
                set.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        thumbView.setAlpha(1f);
                        expandedImageView.setVisibility(View.GONE);
                        currentAnimator = null;
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        thumbView.setAlpha(1f);
                        expandedImageView.setVisibility(View.GONE);
                        currentAnimator = null;
                    }
                });
                set.start();
                currentAnimator = set;
            }
        });
    }
}