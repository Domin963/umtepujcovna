package com.example.umtepujcovna.ui.clients;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;

import com.example.umtepujcovna.AppDatabase;
import com.example.umtepujcovna.R;
import com.example.umtepujcovna.objects.Client;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class ClientListActivity extends AppCompatActivity {
    private AppDatabase db;
    private CustomClientsListAdapter adapter;
    private List<Client> clients;
    private ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_list);
        setTitle("Výběr zákazníka");

        db = AppDatabase.getDatabase(this);
        Thread netThread = new Thread(() -> clients = db.clientDao().getAll());

        //pridani
        FloatingActionButton btnClientAdd = findViewById(R.id.btnClientsListAdd);
        btnClientAdd.setOnClickListener(v -> {
            Intent intent = new Intent(this, ClientAddActivity.class);
            startActivity(intent);
        });

        clients = new ArrayList<>();

        netThread.start();
        listView = findViewById(R.id.clientsListListView);
        adapter = new CustomClientsListAdapter(this, clients);
        listView.setAdapter(adapter);
        registerForContextMenu(listView);

        //vyber zvoleneho klienta
        listView.setOnItemClickListener((parent, view, position, id) -> {
            Client client = (Client) listView.getItemAtPosition(position);
            Intent returnIntent = new Intent();
            returnIntent.putExtra("client", client);
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        });
    }
    public void onResume() {
        super.onResume();
        Thread netThread = new Thread(() -> {
            clients = db.clientDao().getAll();
            runOnUiThread(() -> {
                adapter = new CustomClientsListAdapter(this, clients);
                listView.setAdapter(adapter);
            });
        });
        netThread.start();
    }
}