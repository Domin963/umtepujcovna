package com.example.umtepujcovna.ui.clients;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.umtepujcovna.R;
import com.example.umtepujcovna.objects.Client;

import java.util.List;

public class CustomClientsListAdapter extends BaseAdapter {
    private List<Client> clients;
    private LayoutInflater layoutInflater;

    public CustomClientsListAdapter(Context aContext, List<Client> clients) {
        this.clients = clients;
        this.layoutInflater = LayoutInflater.from(aContext);
    }

    @Override
    public int getCount() {
        return clients.size();
    }

    @Override
    public Object getItem(int position) {
        return clients.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View v, ViewGroup vg) {
        ViewHolder holder;
        if (v == null) {
            v = layoutInflater.inflate(R.layout.clients_list_row, null);
            holder = new ViewHolder();
            holder.uName = v.findViewById(R.id.clientName);
            holder.uStreet = v.findViewById(R.id.clientStreet);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }
        holder.uName.setText(clients.get(position).getFirstName() + " " + clients.get(position).getLastName());
        holder.uStreet.setText(clients.get(position).getAddress().getStreetName()+" "+clients.get(position).getAddress().getHouseNumber()+", "+clients.get(position).getAddress().getCity());
        return v;
    }

    static class ViewHolder {
        TextView uName;
        TextView uStreet;
    }
}
