package com.example.umtepujcovna.ui.inventory;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.umtepujcovna.R;
import com.example.umtepujcovna.objects.InventoryItem;

import java.util.List;

public class CustomListAdapter extends BaseAdapter {

    private List<InventoryItem> items;
    private LayoutInflater layoutInflater;

    public CustomListAdapter(Context aContext, List<InventoryItem> items) {
        this.items = items;
        layoutInflater = LayoutInflater.from(aContext);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View v, ViewGroup vg) {
        ViewHolder holder;
        if (v == null) {
            v = layoutInflater.inflate(R.layout.inventory_list_row, null);
            holder = new ViewHolder();
            holder.uName = v.findViewById(R.id.name);
            holder.uSerial = v.findViewById(R.id.serialNum);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }
        holder.uName.setText(items.get(position).getName());
        holder.uSerial.setText(items.get(position).getSerialNumber());
        return v;
    }

    static class ViewHolder {
        TextView uName;
        TextView uSerial;
    }
}
