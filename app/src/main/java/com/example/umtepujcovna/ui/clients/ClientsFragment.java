package com.example.umtepujcovna.ui.clients;

import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.example.umtepujcovna.AppDatabase;
import com.example.umtepujcovna.R;
import com.example.umtepujcovna.objects.Client;
import com.example.umtepujcovna.objects.ClientWithContracts;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class ClientsFragment extends Fragment {
    private boolean removed = false;
    private AppDatabase db;
    private CustomClientsListAdapter adapter;
    private List<Client> clients;
    private ListView listView;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_clients, container, false);
        db = AppDatabase.getDatabase(getActivity());
        Thread netThread = new Thread(() -> clients = db.clientDao().getAll());

        //pridani
        FloatingActionButton btnClientAdd = root.findViewById(R.id.btnClientsAdd);
        btnClientAdd.setOnClickListener(v -> {
            Intent intent = new Intent(root.getContext(), ClientAddActivity.class);
            startActivity(intent);
        });

        clients = new ArrayList<>();

        netThread.start();
        listView = root.findViewById(R.id.clientsListView);
        adapter = new CustomClientsListAdapter(root.getContext(), clients);
        listView.setAdapter(adapter);
        registerForContextMenu(listView);

        //detail
        listView.setOnItemClickListener((parent, view, position, id) -> {
            Client client = (Client) listView.getItemAtPosition(position);
            Intent intent = new Intent(root.getContext(), ClientDetailActivity.class);
            intent.putExtra("client", client);
            startActivity(intent);
        });
        return root;
    }

    public void onResume() {
        super.onResume();
        Thread netThread = new Thread(() -> {
            clients = db.clientDao().getAll();
            getActivity().runOnUiThread(() -> {
                adapter = new CustomClientsListAdapter(getContext(), clients);
                listView.setAdapter(adapter);
            });
        });
        netThread.start();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (v.getId() == R.id.clientsListView) {
            MenuInflater inflater = getActivity().getMenuInflater();
            inflater.inflate(R.menu.menu_list, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.edit:
                // edit stuff here
                Client clientToEdit = (Client) listView.getItemAtPosition(info.position);
                Intent intent = new Intent(getActivity(), ClientEditActivity.class);
                intent.putExtra("clientToEdit", clientToEdit);
                startActivity(intent);
                return true;
            case R.id.delete:
                // remove stuff here
                Client clientToRemove = (Client) listView.getItemAtPosition(info.position);
                new AlertDialog.Builder(getActivity())
                        .setTitle("Upozornění")
                        .setMessage("Opravdu chcete klienta smazat?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, (dialog, whichButton) -> {
                            Thread netThread = new Thread(() -> {
                                //Pokud ma klient nejake vypujcky, tak nemazat!
                                List<ClientWithContracts> contracts = db.clientDao().getOneClientWithContracts(clientToRemove.getId());
                                if (contracts.get(0).getClient().getId() == clientToRemove.getId() & contracts.get(0).getContractList().size() == 0) {
                                    db.clientDao().delete(clientToRemove);
                                    removed = true;
                                    clients = db.clientDao().getAll();
                                }
                                getActivity().runOnUiThread(() -> {
                                    adapter = new CustomClientsListAdapter(getContext(), clients);
                                    listView.setAdapter(adapter);
                                    if (removed) {
                                        Toast.makeText(getActivity(), "Smazáno", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(getActivity(), "Nelze smazat", Toast.LENGTH_SHORT).show();
                                    }
                                    removed = false;
                                });
                            });
                            netThread.start();
                        })
                        .setNegativeButton(android.R.string.no, null).show();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }
}