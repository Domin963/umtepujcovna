package com.example.umtepujcovna.ui.inventory;

import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.example.umtepujcovna.AppDatabase;
import com.example.umtepujcovna.R;
import com.example.umtepujcovna.objects.InventoryItem;
import com.example.umtepujcovna.objects.InventoryItemWithContracts;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class InventoryFragment extends Fragment {
    private AppDatabase db;
    private List<InventoryItem> items;
    private CustomListAdapter adapter;
    private ListView listView;
    private boolean removed = false;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_inventory, container, false);
        db = AppDatabase.getDatabase(getActivity());
        Thread netThread = new Thread(() -> items = db.inventoryItemDao().getAll());

        //pridani
        FloatingActionButton btnInventoryAdd = root.findViewById(R.id.btnInventoryAdd);
        btnInventoryAdd.setOnClickListener(v -> {
            Intent intent = new Intent(root.getContext(), InventoryAddActivity.class);
            startActivity(intent);
        });

        items = new ArrayList<>();

        netThread.start();
        listView = root.findViewById(R.id.listView);
        adapter = new CustomListAdapter(root.getContext(), items);
        listView.setAdapter(adapter);
        registerForContextMenu(listView);

        //detail
        listView.setOnItemClickListener((parent, view, position, id) -> {
            InventoryItem item = (InventoryItem) listView.getItemAtPosition(position);
            Intent intent = new Intent(root.getContext(), InventoryDetailActivity.class);
            intent.putExtra("item", item);
            startActivity(intent);
        });
        return root;
    }

    public void onResume() {
        super.onResume();
        Thread netThread = new Thread(() -> {
            items = db.inventoryItemDao().getAll();
            getActivity().runOnUiThread(() -> {
                adapter = new CustomListAdapter(getContext(), items);
                listView.setAdapter(adapter);
            });
        });

        netThread.start();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (v.getId() == R.id.listView) {
            MenuInflater inflater = getActivity().getMenuInflater();
            inflater.inflate(R.menu.menu_list, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.edit:
                // edit stuff here
                InventoryItem itemToEdit = (InventoryItem) listView.getItemAtPosition(info.position);
                Intent intent = new Intent(getActivity(), InventoryEditActivity.class);
                intent.putExtra("itemToEdit", itemToEdit);
                startActivity(intent);
                return true;
            case R.id.delete:
                // remove stuff here
                InventoryItem itemToRemove = (InventoryItem) listView.getItemAtPosition(info.position);
                new AlertDialog.Builder(getActivity())
                        .setTitle("Upozornění")
                        .setMessage("Opravdu chcete toto smazat?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, (dialog, whichButton) -> {
                            Thread netThread = new Thread(() -> {
                                List<InventoryItemWithContracts> itemWithContracts = db.inventoryItemDao().getOneInventoryItemWithContracts(itemToRemove.getId());
                                if (itemWithContracts.get(0).getItem().getId() == itemToRemove.getId() && itemWithContracts.get(0).getContractList().size() == 0) {
                                    db.inventoryItemDao().delete(itemToRemove);
                                    removed = true;
                                    items = db.inventoryItemDao().getAll();
                                }
                                getActivity().runOnUiThread(() -> {
                                    adapter = new CustomListAdapter(getContext(), items);
                                    listView.setAdapter(adapter);
                                    if (removed) {
                                        Toast.makeText(getActivity(), "Smazáno", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(getActivity(), "Nelze smazat", Toast.LENGTH_SHORT).show();
                                    }
                                    removed = false;
                                });
                            });
                            netThread.start();
                        })
                        .setNegativeButton(android.R.string.no, null).show();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }
}

