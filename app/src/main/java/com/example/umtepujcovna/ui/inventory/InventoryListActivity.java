package com.example.umtepujcovna.ui.inventory;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.umtepujcovna.AppDatabase;
import com.example.umtepujcovna.R;
import com.example.umtepujcovna.objects.InventoryItem;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class InventoryListActivity extends AppCompatActivity {
    private AppDatabase db;
    private List<InventoryItem> items;
    private CustomListAdapter adapter;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventory_list);

        db = AppDatabase.getDatabase(this);
        Thread netThread = new Thread(() -> items = db.inventoryItemDao().getAll());
        //pridani
        FloatingActionButton btnInventoryAdd = findViewById(R.id.btnListInventoryAdd);
        btnInventoryAdd.setOnClickListener(v -> {
            Intent intent = new Intent(this, InventoryAddActivity.class);
            startActivity(intent);
        });
        items = new ArrayList<>();
        netThread.start();
        listView = findViewById(R.id.inventoryListListView);
        adapter = new CustomListAdapter(this, items);
        listView.setAdapter(adapter);

        //vyber zvoleneho itemu
        listView.setOnItemClickListener((parent, view, position, id) -> {
            InventoryItem item = (InventoryItem) listView.getItemAtPosition(position);
            Intent returnIntent = new Intent();
            returnIntent.putExtra("item", item);
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        });
    }

    public void onResume() {
        super.onResume();
        Thread netThread = new Thread(() -> {
            items = db.inventoryItemDao().getAll();
            runOnUiThread(() -> {
                adapter = new CustomListAdapter(this, items);
                listView.setAdapter(adapter);
            });
        });
        netThread.start();
    }
}